jQuery(document).ready(function ($) {
    $(".call-menu").click(function (event) {
        $("body").toggleClass("is-menuOpen");
        $(this).toggleClass("is-current");
    });
    $(".menu").click(function (event) {
        $("body").removeClass("is-menuOpen");
    });
    $(".menu .inner").click(function (event) {
        event.stopPropagation();
    });

    function showModal(id) {
        $("#" + id).addClass("is-open");

        setTimeout(function () {
            $(document).one("click", function () {
                dismissModal();
            });
        }, 200);

        $(document).on("click", ".modal-inner", function (e) {
            e.stopPropagation();
        });
        $(document).one("click", ".modal__close", function (e) {
            $(document).trigger("click");
        });
    }
    function dismissModal() {
        $(".modal.is-open").removeClass("is-open");
    }

    $(document).on("click", "[data-js='modal__dissmiss']", function (e) {
        dismissModal();
    });

    function headerSpace() {
        var $headerHeight = $(".header").innerHeight();
        var $footerHeight = $(".footer").innerHeight();
        var $miniCartHeight = $(".minicart__inner").height();
        $("body").css("padding-top", $headerHeight);
    }
    $(window).on("resize load", function () {
        headerSpace();
    });

    $(".homeCarousel").slick({
        fade: true,
        dots: true,
        arrows: false,
    });

    $(".productList--slide").slick({
        dots: true,
        arrows: true,
        slidesToShow: 3,
        infinite: false,
        responsive: [
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                    dots: false,
                },
            },
        ],
    });

    $(document).on("click", ".iframe-wrapper", function (ev) {
        ev.preventDefault();
        $(this).addClass("is-playing");
        var videoID = $(this).attr("data-video");

        $(this).append(
            '<iframe width="560" height="315" src="https://www.youtube.com/embed/' +
                videoID +
                '?rel=0&autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>'
        );
    });

    var SPMaskBehavior = function (val) {
            return val.replace(/\D/g, "").length === 11
                ? "(00) 00000-0000"
                : "(00) 0000-00009";
        },
        spOptions = {
            onKeyPress: function (val, e, field, options) {
                field.mask(SPMaskBehavior.apply({}, arguments), options);
            },
        };

    $(".sp_celphones").mask(SPMaskBehavior, spOptions);
    $(".mask--cpf").mask("000.000.000-00", { reverse: true });
    $(".mask--telefone").mask(SPMaskBehavior, spOptions);

    $(".amount").each(function () {
        var _this = $(this);

        var buttonMinus = $(this).find(".amount__input__minus");
        var buttonPlus = $(this).find(".amount__input__plus");
        var buttonNull = $(this).find(".amount__null");

        var input = $(this).find(".amount__input input");

        var amountVal = parseInt(input.val());

        function changeValue(value) {
            input.val(amountVal);
            if (value == 0) {
                $(_this).addClass("is-null");
            } else {
                $(_this).removeClass("is-null");
            }
        }

        changeValue(amountVal);

        $(buttonPlus).on("click", function (e) {
            e.preventDefault();
            amountVal = amountVal + 1;
            changeValue(amountVal);
        });
        $(buttonMinus).on("click", function (e) {
            e.preventDefault();
            amountVal = amountVal - 1;
            changeValue(amountVal);
        });
        $(buttonNull).on("click", function (e) {
            e.preventDefault();
            amountVal = 1;
            changeValue(amountVal);
        });
    });

    $(document).on("click", ".minicart__toggler", function () {
        $(this).parents(".minicart").toggleClass("is-open");
    });

    if ($(".minicart").length > 0) {
        if (window.innerWidth < 768) {
            $("body").css(
                "padding-bottom",
                $(".minicart__toggler").innerHeight()
            );
        }
    }

    $(document).on("click", ".checkoutSuggestion__toggler", function () {
        $(this).parents(".checkoutSuggestion").toggleClass("is-active");
    });
    $(document).on("click", ".accordion__toggler", function () {
        $(this).parents(".accordion").toggleClass("is-open");
    });

    $(".form__field").each(function () {
        $(this)
            .find("input, select, textarea")
            .on("change", function () {
                if ($(this).val() !== "") {
                    $(this).parents(".form__field").addClass("filled");
                } else {
                    $(this).parents(".form__field").removeClass("filled");
                }
            });
    });

    $gallery = $(".gallerySlider").slick({
        fade: true,
        dots: true,
        arrows: true,
    });

    $(document).on(
        "click",
        ".homeSection--local__galeria__thumbs li",
        function () {
            var idx = $(this).index(".homeSection--local__galeria__thumbs li");
            showModal("modal-gallery");
            $gallery.slick("goTo", idx);
        }
    );
});
