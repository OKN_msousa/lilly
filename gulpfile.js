const { watch, src, dest } = require('gulp');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');
const rename = require("gulp-rename");
const imagemin = require('gulp-imagemin');



function updateCss() {
  return src('css/style.min.css')
  .pipe(dest('../www/assets/css'));
}

function updateJS() {
  return src(['js/jquery.js', 'js/slick/slick.js', 'js/site.js'])
  .pipe(uglify())
  .pipe(concat('bundle.js')) 
  //.pipe(dest('js'));
  .pipe(dest('../www/assets/js'));
}

function updateSpriteSVG() {
  return src('images/sprite.svg')
  .pipe(rename(function(path){
    path.extname = ".svg.php";
  }))
  .pipe(dest('../www/assets/images'))
}


function optimizeImages() {
  return src('images/**/*')
  .pipe(imagemin())
  .pipe(dest('../www/assets/images'))
}

exports.default = function() {
  updateJS();
  updateCss();
  //optimizeImages();
  watch('css/style.min.css', updateCss);
  watch('js/*.js', updateJS);
}
 
exports.sprite = updateSpriteSVG;